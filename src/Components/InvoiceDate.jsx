import React from "react";

import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

function InvoiceDate({
  title,
  actionType,
  name,
  selected,
  handleEvent,
  invoiceDate,
}) {
  return (
    <div className="col-6">
      <div className="row">
        <div className="col-3 d-flex justify-content-between align-items-center py-2">
          <h6 className="m-0" style={{ textAlign: "left" }}>
            {title}
          </h6>
        </div>
        <div className="col-4 d-flex align-items-center py-2 px-0">
          <DatePicker
            selected={selected}
            onSelect={(date) =>
              handleEvent({
                type: actionType,
                payload: { [name]: date },
              })
            }
            dateFormat="MMMM d, yyyy"
          />
        </div>
      </div>
      <div className="col-8 offset-3 d-flex justify-content-between align-items-center py-0">
        {invoiceDate - selected > 60*60*23 && actionType === "invoiceDueDate" && (
          <small className="text-danger" style={{ textAlign: "left" }}>
            * Select Valid date. Due date should not be less than the
            Invoice date
          </small>
        )}
      </div>
    </div>
  );
}

export default InvoiceDate;
