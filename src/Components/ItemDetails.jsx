import React from "react";

function ItemDetails({ item, handleDelete, handleEvent, errorDetailsOfItems }) {
  return (
    <div className="row">
      <div className="col-1">
        <button
          className="py-2 px-3"
          style={{ borderRadius: "5px" }}
          onClick={() =>
            handleDelete({ type: "deleteItem", payload: { id: item.id } })
          }
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-x"
            viewBox="0 0 16 16"
          >
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </button>
      </div>
      <div className="col-4">
        <div className="row">
          <div className="col-12">
            <input
              type="text"
              className="w-100 p-2"
              placeholder="Item Description"
              value={item.description}
              onChange={(e) =>
                handleEvent({
                  type: "itemDescription",
                  payload: { value: e.target.value, id: item.id },
                })
              }
            ></input>
          </div>
          <div className="col-12 textAlignLeft text-danger">
            {errorDetailsOfItems[item.id] &&
              errorDetailsOfItems[item.id].isItemDescriptionValid === false && (
                <small>
                  * Not Valid Description, shoud be minimum Five Characters
                </small>
              )}
            {errorDetailsOfItems[item.id] &&
              errorDetailsOfItems[item.id].isItemDescriptionNotEmpty ===
                false && <small>* Enter Item Description</small>}
          </div>
        </div>
      </div>
      <div className="col-2">
        <div className="row">
          <div className="col-12">
            <input
              type="number"
              className="w-100 p-2"
              style={{ textAlign: "right" }}
              placeholder="Price"
              value={item.price}
              onChange={(e) =>
                handleEvent({
                  type: "itemPrice",
                  payload: { value: e.target.value, id: item.id },
                })
              }
            ></input>
          </div>
          <div className="col-12 textAlignLeft text-danger">
            {errorDetailsOfItems[item.id] &&
              errorDetailsOfItems[item.id].isItemPriceValid === false && (
                <small>* Price Should be positive</small>
              )}
            {errorDetailsOfItems[item.id] &&
              errorDetailsOfItems[item.id].isItemPriceNotEmpty === false && (
                <small>* Enter Price</small>
              )}
          </div>
        </div>
      </div>
      <div className="col-2">
        <div className="row">
          <div className="col-12">
            <input
              type="number"
              className="w-100 p-2"
              style={{ textAlign: "right" }}
              placeholder="Qty"
              value={item.quantity}
              onChange={(e) =>
                handleEvent({
                  type: "itemQuantity",
                  payload: { value: e.target.value, id: item.id },
                })
              }
            ></input>
          </div>
          <div className="col-12  textAlignLeft text-danger">
            {errorDetailsOfItems[item.id] &&
              errorDetailsOfItems[item.id].isItemQuantityValid === false && (
                <small>* Qty Should be positive</small>
              )}
            {errorDetailsOfItems[item.id] &&
              errorDetailsOfItems[item.id].isItemQuantityNotEmpty === false && (
                <small>* Enter Quantity</small>
              )}
          </div>
        </div>
      </div>
      <div className="col-2 d-flex justify-content-end align-items-center">
        <p className="m-0">
          ₹{" "}
          {isNaN(
            (parseFloat(item.price) * parseFloat(item.quantity)).toFixed(2)
          )
            ? "0.00"
            : (parseFloat(item.price) * parseFloat(item.quantity)).toFixed(2)}
        </p>
      </div>
      <div className="col-1 d-flex align-items-center">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="30"
          height="30"
          fill="white"
          className="bi bi-check2 p-1"
          viewBox="0 0 16 16"
          style={{ backgroundColor: "black", borderRadius: "5px" }}
        >
          <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
        </svg>
      </div>
      <div className="col-4 offset-1 d-felx py-2">
        <textarea
          className="w-100 p-2"
          rows={3}
          placeholder="Additional Details"
          value={item.additionalDetails}
          onChange={(e) =>
            handleEvent({
              type: "itemAdditionalDetails",
              payload: { value: e.target.value, id: item.id },
            })
          }
        ></textarea>
      </div>
      <hr style={{ borderTop: "dotted 1px" }}></hr>
    </div>
  );
}

export default ItemDetails;
