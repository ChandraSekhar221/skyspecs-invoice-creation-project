import { v4 as uuid } from "uuid";

import InitialInvoiceDueDate from "./InitialInvoiceDueDate";

const today = new Date();

const InitialState = {
  invoiceTitle: "INVOICE",
  imageDetails: null,
  from: {},
  to: {},
  invoiceDate: today,
  invoiceNumber: "INV0001",
  invoiceTerms: "Due On Receipt",
  invoiceDueDate: InitialInvoiceDueDate(),
  itemDetails: [
    {
      id: uuid(),
      description: "Item 1",
      price: 10,
      quantity: 1,
      additionalDetails: "",
    },
  ],
  errorDetails: {
    invoiceTitleIsEmpty: true,
    fromNameIsNotEmpty: "",
    fromNameValid: "",
    fromEmailIsNotEmpty: "",
    fromEmailValid: "",
    fromAddressIsNotEmpty: "",
    fromAddressValid: "",
    fromCityStateIsNotEmpty: "",
    fromCityStateValid: "",
    fromPincodeIsNotEmpty: "",
    fromPincodeValid: "",
    fromPhoneIsNotEmpty: "",
    fromPhoneValid: "",
    fromBusinessNumberIsNotEmpty: "",
    fromBusinessNumberValid: "",
    toNameIsNotEmpty: "",
    toNameValid: "",
    toEmailIsNotEmpty: "",
    toEmailValid: "",
    toAddressIsNotEmpty: "",
    toAddressValid: "",
    toCityStateIsNotEmpty: "",
    toCityStateValid: "",
    toPincodeIsNotEmpty: "",
    toPincodeValid: "",
    toPhoneIsNotEmpty: "",
    toPhoneValid: "",
    invoiceNumberIsNotEmpty: true,
    invoiceNumberValid: true,
  },
  errorDetailsOfItems: {},
  dataSuccessFull: false,
};

export default InitialState;
