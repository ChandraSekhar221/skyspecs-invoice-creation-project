import React from "react";

import { InputFocusStyles, ErrorMsgDetails } from "./Constants";

function AddressInput({
  name,
  value,
  handleEvent,
  actionType,
  placeHolder,
  textEmptyErr,
  textValidErr,
}) {
  console.log(ErrorMsgDetails[actionType + name]);
  const style = `w-100 py-1 px-2 ${
    textValidErr === false ? InputFocusStyles.error : InputFocusStyles.normal
  }`;
  return (
    <div className="row">
      {actionType === "to" && <div className="col-1"></div>}
      <div className="col-3 d-flex justify-content-between align-items-center py-2 ">
        <h6 className="m-0" style={{ textAlign: "left" }}>
          {name[0].toUpperCase() + name.slice(1)}
        </h6>
      </div>
      <div className="col-8 d-flex justify-content-between align-items-center pt-2 px-0">
        <input
          className={style}
          type="text"
          name={name}
          onChange={(e) =>
            handleEvent({
              type: actionType,
              payload: { value: e.target.value, name: name },
            })
          }
          value={value}
          placeholder={placeHolder}
          size="20"
        ></input>
      </div>
      {actionType === "from" && <div className="col-1"></div>}
      {textValidErr === false && (
        <div className="col-12">
          <div className="row">
            {actionType === "to" && <div className="col-1"></div>}
            <div className="offset-3 col-8 d-flex px-0">
              <small className="text-danger textAlignLeft">
                {ErrorMsgDetails[actionType + name]}
              </small>
            </div>
            {actionType === "from" && <div className="col-1"></div>}
          </div>
        </div>
      )}
      {textEmptyErr === false && (
        <div className="col-12">
          <div className="row">
            {actionType === "to" && <div className="col-1"></div>}
            <div className="offset-3 col-8 d-flex px-0">
              <small className="text-danger textAlignLeft">
                *Enter {name[0].toUpperCase() + name.slice(1)}
              </small>
            </div>
            {actionType === "from" && <div className="col-1"></div>}
          </div>
        </div>
      )}
    </div>
  );
}

export default AddressInput;
