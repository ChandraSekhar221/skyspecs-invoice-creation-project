import React, { useReducer } from "react";
import AddressInput from "./AddressInput";
import AddressPopup from "./AddressPopup";
import TextInput from "./TextInput";
import InvoiceDate from "./InvoiceDate";
import InvoiceDueDateTerms from "./InvoiceDueDateTerms";
import ItemDetails from "./ItemDetails";
import SuccessMsgDetails from "./SuccessMsgDetails";
import HorizontalLine from "./HorizontalLine";
import { v4 as uuid } from "uuid";

import LogoDropDown from "./LogoDropDown";

import "./Style.css";

import Reducer from "./Reducer";

import InitialState from "./State";

import { HrStyles } from "./Constants";

function Invoive() {
  const [state, dispatch] = useReducer(Reducer, InitialState);

  const handleInput = (action) => {
    return dispatch(action);
  };
  const handleDeleteItem = (action) => {
    return dispatch(action);
  };

  const handleCreateItem = (action) => {
    return dispatch(action);
  };

  const handleSubmit = (action) => {
    return dispatch(action);
  };
  return (
    <div className="p-2">
      <h2>Invoice Model</h2>
      <hr />
      <section className="container">
        <div className="row" name="Top">
          <div className="col-6 d-flex">
            <TextInput
              value={state.invoiceTitle.toUpperCase()}
              height="3rem"
              padding="1rem"
              handleEvent={handleInput}
              actionType="invoiceTitile"
              invoiceTitleErr={state.errorDetails.invoiceTitleIsEmpty}
            />
          </div>
          <div className="col-6 d-flex justify-content-end">
            <LogoDropDown
              handleEvent={handleInput}
              imageDetails={state.imageDetails}
            />
          </div>
        </div>
        <div className="row pt-5" name="AddressTitle">
          <div className="col-6 d-flex align-items-center">
            <div className="row">
              <div className="col-11">
                <h5 className="m-0 py-1">From</h5>
              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="row">
              <div className="col-11 offset-1 d-flex align-items-center">
                <h5 className="m-0 py-1">Bill To</h5>
              </div>
            </div>
          </div>
        </div>
        <div className="row" name="Address Details">
          <div className="col-6" name="AddressInput">
            <AddressInput
              name="name"
              placeHolder="Enter Name"
              handleEvent={handleInput}
              actionType="from"
              value={state.from.name || ""}
              textEmptyErr={state.errorDetails.fromNameIsNotEmpty}
              textValidErr={state.errorDetails.fromNameValid}
            />
            <AddressInput
              name="email"
              placeHolder="xyz@gmail.com"
              handleEvent={handleInput}
              actionType="from"
              value={state.from.email || ""}
              textEmptyErr={state.errorDetails.fromEmailIsNotEmpty}
              textValidErr={state.errorDetails.fromEmailValid}
            />
            <AddressInput
              name="address"
              placeHolder="Enter Address"
              handleEvent={handleInput}
              actionType="from"
              value={state.from.address || ""}
              textEmptyErr={state.errorDetails.fromAddressIsNotEmpty}
              textValidErr={state.errorDetails.fromAddressValid}
            />
            {state.from.address && (
              <>
                <AddressPopup
                  name="cityState"
                  placeHolder="Enter City, State"
                  handleEvent={handleInput}
                  actionType="from"
                  value={state.from.cityState || ""}
                  textEmptyErr={state.errorDetails.fromCityStateIsNotEmpty}
                  textValidErr={state.errorDetails.fromCityStateValid}
                />
                <AddressPopup
                  name="pincode"
                  placeHolder="Enter Pincode"
                  handleEvent={handleInput}
                  actionType="from"
                  value={state.from.pincode || ""}
                  textEmptyErr={state.errorDetails.fromPincodeIsNotEmpty}
                  textValidErr={state.errorDetails.fromPincodeValid}
                />
              </>
            )}
            <AddressInput
              name="phone"
              placeHolder="Enter Phone Number"
              handleEvent={handleInput}
              actionType="from"
              value={state.from.phone || ""}
              textEmptyErr={state.errorDetails.fromPhoneIsNotEmpty}
              textValidErr={state.errorDetails.fromPhoneValid}
            />
            <AddressInput
              name="businessNumber"
              placeHolder="Enter Business Number"
              handleEvent={handleInput}
              actionType="from"
              value={state.from.businessNumber || ""}
              textEmptyErr={state.errorDetails.fromBusinessNumberIsNotEmpty}
              textValidErr={state.errorDetails.fromBusinessNumberValid}
            />
          </div>
          <div className="col-6" name="toComponent">
            <AddressInput
              name="name"
              placeHolder="Enter Name"
              handleEvent={handleInput}
              actionType="to"
              value={state.to.name || ""}
              textEmptyErr={state.errorDetails.toNameIsNotEmpty}
              textValidErr={state.errorDetails.toNameValid}
            />
            <AddressInput
              name="email"
              placeHolder="xyz@gmail.com"
              handleEvent={handleInput}
              actionType="to"
              value={state.to.email || ""}
              textEmptyErr={state.errorDetails.toEmailIsNotEmpty}
              textValidErr={state.errorDetails.toEmailValid}
            />
            <AddressInput
              name="address"
              placeHolder="Enter Address"
              handleEvent={handleInput}
              actionType="to"
              value={state.to.address || ""}
              textEmptyErr={state.errorDetails.toAddressIsNotEmpty}
              textValidErr={state.errorDetails.toAddressValid}
            />
            {state.to.address && (
              <>
                <AddressPopup
                  name="cityState"
                  placeHolder="Enter City, State"
                  handleEvent={handleInput}
                  actionType="to"
                  value={state.to.cityState || ""}
                  textEmptyErr={state.errorDetails.toCityStateIsNotEmpty}
                  textValidErr={state.errorDetails.toCityStateValid}
                />
                <AddressPopup
                  name="pincode"
                  placeHolder="Enter Pincode"
                  handleEvent={handleInput}
                  actionType="to"
                  value={state.to.pincode || ""}
                  textEmptyErr={state.errorDetails.toPincodeIsNotEmpty}
                  textValidErr={state.errorDetails.toPincodeValid}
                />
              </>
            )}
            <AddressInput
              name="phone"
              placeHolder="Enter Phone Number"
              handleEvent={handleInput}
              actionType="to"
              value={state.to.phone || ""}
              textEmptyErr={state.errorDetails.toPhoneIsNotEmpty}
              textValidErr={state.errorDetails.toPhoneValid}
            />
          </div>
        </div>
        <HorizontalLine type={HrStyles.normal} />
        <div className="row" name="invoiceNumber">
          <div className="col-6">
            <AddressInput
              name="invoiceNumber"
              placeHolder="INV0001"
              handleEvent={handleInput}
              actionType="invoiceNumber"
              value={state.invoiceNumber}
              textEmptyErr={state.errorDetails.invoiceNumberIsNotEmpty}
              textValidErr={state.errorDetails.invoiceNumberValid}
            />
          </div>
        </div>
        <div className="row" name="invoiceDate">
          <InvoiceDate
            selected={state.invoiceDate}
            title="Date"
            name="invoiceDate"
            actionType="invoiceDate"
            handleEvent={handleInput}
          />
        </div>
        <div className="row" name="invoiceTerms">
          <InvoiceDueDateTerms
            title="Terms"
            name="invoiceTerms"
            actionType="invoiceTerms"
            handleEvent={handleInput}
          />
          <br></br>
          <br></br>
        </div>
        <div className="row" name="due date">
          {state.invoiceTerms !== "Due On Receipt" &&
            state.invoiceTerms !== "None" && (
              <InvoiceDate
                invoiceTermsValue={state.invoiceTerms}
                selected={state.invoiceDueDate}
                invoiceDate={state.invoiceDate}
                title="Due Date"
                name="invoiceDueDate"
                actionType="invoiceDueDate"
                handleEvent={handleInput}
              />
            )}
        </div>
        <HorizontalLine type={HrStyles.solid} />
        <div className="row pb-2" name="invoiceHeading">
          <div className="col-4 offset-1 d-flex">
            <h6 className="m-0">DESCRIPTION</h6>
          </div>
          <div className="col-2 d-flex justify-content-end">
            <h6 className="m-0">RATE</h6>
          </div>
          <div className="col-2 d-flex  justify-content-end">
            <h6 className="m-0">QTY</h6>
          </div>
          <div className="col-2 d-flex justify-content-end">
            <h6 className="m-0">AMOUNT</h6>
          </div>
          <div className="col-1 d-flex">
            <h6 className="m-0">TAX</h6>
          </div>
        </div>
        <HorizontalLine type={HrStyles.solid} />
        <div name="Alert Message of list Items">
          {!state.itemDetails.length && (
            <div className="row">
              <div className="col d-flex">
                <h5 className="text-danger">
                  * Items Should not be empty. Add one or more items to generate
                  Invoice
                </h5>
              </div>
            </div>
          )}
          {state.itemDetails.map((eachItem) => {
            return (
              <ItemDetails
                key={eachItem.id}
                item={eachItem}
                handleDelete={handleDeleteItem}
                handleEvent={handleInput}
                errorDetailsOfItems={state.errorDetailsOfItems}
              />
            );
          })}
        </div>
        <div className="row pb-2" name="addItemToInvoiceBtn">
          <div className="col-1">
            <button
              className="py-2 px-3"
              style={{ borderRadius: "5px" }}
              onClick={() =>
                handleCreateItem({
                  type: "createItem",
                  payload: {
                    id: uuid(),
                    description: "Itme -1",
                    price: 10,
                    quantity: 1,
                    additionalDetails: "",
                  },
                })
              }
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="currentColor"
                className="bi bi-plus-square-fill"
                viewBox="0 0 16 16"
              >
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z" />
              </svg>
            </button>
          </div>
        </div>
        <HorizontalLine type={HrStyles.normal} />
        <div className="row" name="total amounts">
          <div className="col-12">
            <div className="row">
              <div className="col-5 offset-5 d-flex justify-content-end">
                <p>Subtotal</p>
              </div>
              <div className="col-1 d-flex justify-content-end">
                <p>
                  {state.itemDetails
                    .reduce((prevValue, item) => {
                      let value =
                        parseFloat(item.price) * parseFloat(item.quantity);
                      if (isNaN(value)) {
                        value = 0;
                      }
                      prevValue += value;
                      return prevValue;
                    }, 0)
                    .toFixed(2)}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="row" name="generateInvoice">
          <div className="col-12 d-flex py-2 justify-content-center align-items-center">
            <button
              className="btn btn-primary"
              onClick={() => handleSubmit({ type: "dataCheck" })}
            >
              Generate Invoice
            </button>
          </div>
        </div>
        <HorizontalLine type={HrStyles.normal} />
        <div className="row" name="Success Invoice Generator">
          {state.dataSuccessFull && <SuccessMsgDetails state={state} />}
        </div>
      </section>
    </div>
  );
}

export default Invoive;
