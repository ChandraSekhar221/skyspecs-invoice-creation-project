import React from "react";

function TextInput({
  type,
  name,
  value,
  height,
  padding,
  actionType,
  handleEvent,
  invoiceTitleErr,
}) {
  return (
    <div>
      <input
        className="w-100"
        type={type}
        name={name}
        defaultValue={value}
        onChange={(e) =>
          handleEvent({
            type: actionType,
            payload: { value: e.target.value },
          })
        }
        size="20"
        style={{ height: height, padding: padding }}
      ></input>
      {invoiceTitleErr === false && (
        <small className="d-flex text-danger">
          *Enter Invoice Title, Should not be Empty
        </small>
      )}
    </div>
  );
}

export default TextInput;
