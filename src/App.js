import './App.css';
import Invoive from './Components/Invoive';

function App() {
  return (
    <div className="App">
      <Invoive />
    </div>
  );
}

export default App;
