import React from "react";

import { InputFocusStyles } from "./Constants";

function AddressPopup({
  name,
  placeHolder,
  handleEvent,
  actionType,
  value,
  textEmptyErr,
  textValidErr,
}) {
  const style = `w-100 py-1 px-2 ${
    textValidErr === false ? InputFocusStyles.error : InputFocusStyles.normal
  }`;
  return (
    <div className="row">
      {actionType === "to" && <div className="col-1"></div>}
      <div className="col-8 offset-3 d-flex justify-content-between align-items-center pt-2 px-0">
        <input
          className={style}
          type="text"
          name={name}
          onChange={(e) =>
            handleEvent({
              type: actionType,
              payload: { value: e.target.value, name: name },
            })
          }
          value={value || ""}
          placeholder={placeHolder}
          style={{ height: "2rem", padding: "1rem" }}
        ></input>
      </div>
      {actionType === "from" && <div className="col-1"></div>}
      <div className="col-12">
        <div className="row">
          {actionType === "to" && <div className="col-1"></div>}
          <div className="col-8 offset-3  d-flex align-items-center px-0 textAlignLeft">
            {textEmptyErr === false &&
              (value === undefined || value === "") && (
                <small className="text-danger">*{placeHolder}</small>
              )}
            {textValidErr === false && name === "cityState" && (
              <small className="text-danger">
                *City State should be of minimum Five Characters
              </small>
            )}
            {textValidErr === false && name === "pincode" && (
              <small className="text-danger">
                *Pincode should be a number of Six digits
              </small>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddressPopup;
