import React from "react";

function InvoiceDueDateTerms({ title, name, handleEvent, actionType }) {
  return (
    <div className="col-6">
      <div className="row">
        <div className="col-3 d-flex justify-content-between align-items-center py-2">
          <h6 className="m-0" style={{ textAlign: "left" }}>
            {title}
          </h6>
        </div>
        <div className="col-5 d-flex justify-content-between align-items-center py-2 px-0">
          <select
            className="w-100 py-2"
            name={title}
            defaultValue='Due On Receipt'
            onChange={(e) =>
              handleEvent({
                type: actionType,
                payload: { [name]: e.target.value },
              })
            }
          >
            <option value="None">None</option>
            <option value="Custom">Custom</option>
            <option value="Due On Receipt">
              Due On Receipt
            </option>
            <option value="Next Day">Next Day</option>
            <option value="2 Days">2 Days</option>
            <option value="3 Days">3 Days</option>
            <option value="4 Days">4 Days</option>
            <option value="5 Days">5 Days</option>
            <option value="6 Days">6 Days</option>
            <option value="7 Days">7 Days</option>
          </select>
        </div>
      </div>
    </div>
  );
}

export default InvoiceDueDateTerms;
