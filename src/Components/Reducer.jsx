import validator from "validator";

import InitialInvoiceDueDate from "./InitialInvoiceDueDate";

// const addressErrorTypes = {
//   nameEmpty: "nameEmpty",
//   nameValidate: "nameValidate",
//   emailEmpty: "emailEmpty",
// };

const Reducer = (state, action) => {
  switch (action.type) {
    case "invoiceTitile":
      if (action.payload.value.trim()) {
        return {
          ...state,
          invoiceTitle: action.payload.value.toUpperCase(),
          errorDetails: { ...state.errorDetails, invoiceTitleIsEmpty: true },
        };
      }
      return {
        ...state,
        invoiceTitle: action.payload.value.toUpperCase(),
        errorDetails: { ...state.errorDetails, invoiceTitleIsEmpty: false },
      };
    case "dragAndDropImage":
      action.payload["url"] = URL.createObjectURL(action.payload);
      return {
        ...state,
        imageDetails: action.payload,
      };
    case "deleteImage":
      return {
        ...state,
        imageDetails: null,
      };
    case "from":
      switch (action.payload.name) {
        case "name":
          if (action.payload.value.trim()) {
            if (validator.isAlpha(action.payload.value.trim())) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromNameIsNotEmpty: true,
                  fromNameValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromNameIsNotEmpty: true,
                fromNameValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromNameIsNotEmpty: false,
              fromNameValid: "",
            },
          };
        case "email":
          if (action.payload.value.trim()) {
            if (validator.isEmail(action.payload.value.trim())) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromEmailIsNotEmpty: true,
                  fromEmailValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromEmailIsNotEmpty: true,
                fromEmailValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromEmailIsNotEmpty: false,
              fromEmailValid: "",
            },
          };
        case "address":
          if (action.payload.value.trim()) {
            if (action.payload.value.trim().length >= 5) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromAddressIsNotEmpty: true,
                  fromAddressValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromAddressIsNotEmpty: true,
                fromAddressValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddressIsNotEmpty: false,
              fromAddressValid: "",
            },
          };
        case "cityState":
          if (action.payload.value.trim()) {
            if (action.payload.value.trim().length >= 5) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromCityStateIsNotEmpty: true,
                  fromCityStateValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromCityStateIsNotEmpty: true,
                fromCityStateValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromCityStateIsNotEmpty: false,
              fromCityStateValid: "",
            },
          };
        case "pincode":
          if (action.payload.value.trim()) {
            if (
              validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 6
            ) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromPincodeIsNotEmpty: true,
                  fromPincodeValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromPincodeIsNotEmpty: true,
                fromPincodeValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromPincodeIsNotEmpty: false,
              fromPincodeValid: "",
            },
          };
        case "phone":
          if (action.payload.value.trim()) {
            if (
              validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 10
            ) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromPhoneIsNotEmpty: true,
                  fromPhoneValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromPhoneIsNotEmpty: true,
                fromPhoneValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromPhoneIsNotEmpty: false,
              fromPhoneValid: "",
            },
          };
        case "businessNumber":
          if (action.payload.value.trim()) {
            if (
              validator.isAlphanumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length >= 6
            ) {
              return {
                ...state,
                from: {
                  ...state.from,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  fromBusinessNumberIsNotEmpty: true,
                  fromBusinessNumberValid: true,
                },
              };
            }
            return {
              ...state,
              from: {
                ...state.from,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                fromBusinessNumberIsNotEmpty: true,
                fromBusinessNumberValid: false,
              },
            };
          }
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromBusinessNumberIsNotEmpty: false,
              fromBusinessNumberValid: "",
            },
          };
        default:
          return {
            ...state,
          };
      }
    case "to":
      switch (action.payload.name) {
        case "name":
          if (action.payload.value.trim()) {
            if (validator.isAlpha(action.payload.value.trim())) {
              return {
                ...state,
                to: {
                  ...state.to,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  toNameIsNotEmpty: true,
                  toNameValid: true,
                },
              };
            }
            return {
              ...state,
              to: {
                ...state.to,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                toNameIsNotEmpty: true,
                toNameValid: false,
              },
            };
          }
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toNameIsNotEmpty: false,
              toNameValid: "",
            },
          };
        case "email":
          if (action.payload.value.trim()) {
            if (validator.isEmail(action.payload.value.trim())) {
              return {
                ...state,
                to: {
                  ...state.to,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  toEmailIsNotEmpty: true,
                  toEmailValid: true,
                },
              };
            }
            return {
              ...state,
              to: {
                ...state.to,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                toEmailIsNotEmpty: true,
                toEmailValid: false,
              },
            };
          }
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toEmailIsNotEmpty: false,
              toEmailValid: "",
            },
          };
        case "address":
          if (action.payload.value.trim()) {
            if (action.payload.value.trim().length >= 5) {
              return {
                ...state,
                to: {
                  ...state.to,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  toAddressIsNotEmpty: true,
                  toAddressValid: true,
                },
              };
            }
            return {
              ...state,
              to: {
                ...state.to,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                toAddressIsNotEmpty: true,
                toAddressValid: false,
              },
            };
          }
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddressIsNotEmpty: false,
              toAddressValid: "",
            },
          };
        case "cityState":
          if (action.payload.value.trim()) {
            if (action.payload.value.trim().length >= 5) {
              return {
                ...state,
                to: {
                  ...state.to,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  toCityStateIsNotEmpty: true,
                  toCityStateValid: true,
                },
              };
            }
            return {
              ...state,
              to: {
                ...state.to,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                toCityStateIsNotEmpty: true,
                toCityStateValid: false,
              },
            };
          }
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toCityStateIsNotEmpty: false,
              toCityStateValid: "",
            },
          };
        case "pincode":
          if (action.payload.value.trim()) {
            if (
              validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 6
            ) {
              return {
                ...state,
                to: {
                  ...state.to,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  toPincodeIsNotEmpty: true,
                  toPincodeValid: true,
                },
              };
            }
            return {
              ...state,
              to: {
                ...state.to,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                toPincodeIsNotEmpty: true,
                toPincodeValid: false,
              },
            };
          }
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toPincodeIsNotEmpty: false,
              toPincodeValid: "",
            },
          };
        case "phone":
          if (action.payload.value.trim()) {
            if (
              validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 10
            ) {
              return {
                ...state,
                to: {
                  ...state.to,
                  [action.payload.name]: action.payload.value,
                },
                errorDetails: {
                  ...state.errorDetails,
                  toPhoneIsNotEmpty: true,
                  toPhoneValid: true,
                },
              };
            }
            return {
              ...state,
              to: {
                ...state.to,
                [action.payload.name]: action.payload.value,
              },
              errorDetails: {
                ...state.errorDetails,
                toPhoneIsNotEmpty: true,
                toPhoneValid: false,
              },
            };
          }
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toPhoneIsNotEmpty: false,
              toPhoneValid: "",
            },
          };
        default:
          return {
            ...state,
          };
      }
    case "invoiceNumber":
      if (action.payload.value.trim()) {
        if (
          validator.isAlphanumeric(action.payload.value.trim()) &&
          action.payload.value.trim().length >= 7
        ) {
          return {
            ...state,
            invoiceNumber: action.payload.value,
            errorDetails: {
              ...state.errorDetails,
              invoiceNumberIsNotEmpty: true,
              invoiceNumberValid: true,
            },
          };
        }
        return {
          ...state,
          invoiceNumber: action.payload.value,
          errorDetails: {
            ...state.errorDetails,
            invoiceNumberIsNotEmpty: true,
            invoiceNumberValid: false,
          },
        };
      }
      return {
        ...state,
        invoiceNumber: action.payload.value,
        errorDetails: {
          ...state.errorDetails,
          invoiceNumberIsNotEmpty: false,
          invoiceNumberValid: "",
        },
      };
    case "invoiceDate":
      return {
        ...state,
        invoiceDueDate: InitialInvoiceDueDate(
          action.payload.invoiceDate,
          state.invoiceTerms
        ),
        ...action.payload,
      };
    case "invoiceTerms":
      return {
        ...state,
        invoiceDueDate: InitialInvoiceDueDate(
          state.invoiceDate,
          action.payload.invoiceTerms
        ),
        ...action.payload,
      };
    case "invoiceDueDate":
      return {
        ...state,
        ...action.payload,
      };
    case "itemDescription":
      if (action.payload.value.trim()) {
        if (action.payload.value.trim().length >= 5) {
          return {
            ...state,
            itemDetails: state.itemDetails.map((eachItem) => {
              if (eachItem.id === action.payload.id) {
                eachItem.description = action.payload.value;
                return eachItem;
              }
              return eachItem;
            }),
            errorDetailsOfItems: {
              ...state.errorDetailsOfItems,
              [action.payload.id]: {
                ...state.errorDetailsOfItems[action.payload.id],
                isItemDescriptionValid: true,
                isItemDescriptionNotEmpty: true,
              },
            },
          };
        }
        return {
          ...state,
          itemDetails: state.itemDetails.map((eachItem) => {
            if (eachItem.id === action.payload.id) {
              eachItem.description = action.payload.value;
              return eachItem;
            }
            return eachItem;
          }),
          errorDetailsOfItems: {
            ...state.errorDetailsOfItems,
            [action.payload.id]: {
              ...state.errorDetailsOfItems[action.payload.id],
              isItemDescriptionValid: false,
              isItemDescriptionNotEmpty: true,
            },
          },
        };
      }
      return {
        ...state,
        itemDetails: state.itemDetails.map((eachItem) => {
          if (eachItem.id === action.payload.id) {
            eachItem.description = action.payload.value;
            return eachItem;
          }
          return eachItem;
        }),
        errorDetailsOfItems: {
          ...state.errorDetailsOfItems,
          [action.payload.id]: {
            ...state.errorDetailsOfItems[action.payload.id],
            isItemDescriptionValid: "",
            isItemDescriptionNotEmpty: false,
          },
        },
      };
    case "itemPrice":
      if (action.payload.value.trim()) {
        if (parseFloat(action.payload.value.trim()) >= 0) {
          return {
            ...state,
            itemDetails: state.itemDetails.map((eachItem) => {
              if (eachItem.id === action.payload.id) {
                eachItem.price = action.payload.value;
                return eachItem;
              }
              return eachItem;
            }),
            errorDetailsOfItems: {
              ...state.errorDetailsOfItems,
              [action.payload.id]: {
                ...state.errorDetailsOfItems[action.payload.id],
                isItemPriceValid: true,
                isItemPriceNotEmpty: true,
              },
            },
          };
        }
        return {
          ...state,
          itemDetails: state.itemDetails.map((eachItem) => {
            if (eachItem.id === action.payload.id) {
              eachItem.price = action.payload.value;
              return eachItem;
            }
            return eachItem;
          }),
          errorDetailsOfItems: {
            ...state.errorDetailsOfItems,
            [action.payload.id]: {
              ...state.errorDetailsOfItems[action.payload.id],
              isItemPriceValid: false,
              isItemPriceNotEmpty: true,
            },
          },
        };
      }
      return {
        ...state,
        itemDetails: state.itemDetails.map((eachItem) => {
          if (eachItem.id === action.payload.id) {
            eachItem.price = action.payload.value;
            return eachItem;
          }
          return eachItem;
        }),
        errorDetailsOfItems: {
          ...state.errorDetailsOfItems,
          [action.payload.id]: {
            ...state.errorDetailsOfItems[action.payload.id],
            isItemPriceValid: "",
            isItemPriceNotEmpty: false,
          },
        },
      };
    case "itemQuantity":
      if (action.payload.value.trim()) {
        if (parseFloat(action.payload.value.trim()) >= 0) {
          return {
            ...state,
            itemDetails: state.itemDetails.map((eachItem) => {
              if (eachItem.id === action.payload.id) {
                eachItem.quantity = action.payload.value;
                return eachItem;
              }
              return eachItem;
            }),
            errorDetailsOfItems: {
              ...state.errorDetailsOfItems,
              [action.payload.id]: {
                ...state.errorDetailsOfItems[action.payload.id],
                isItemQuantityValid: true,
                isItemQuantityNotEmpty: true,
              },
            },
          };
        }
        return {
          ...state,
          itemDetails: state.itemDetails.map((eachItem) => {
            if (eachItem.id === action.payload.id) {
              eachItem.quantity = action.payload.value;
              return eachItem;
            }
            return eachItem;
          }),
          errorDetailsOfItems: {
            ...state.errorDetailsOfItems,
            [action.payload.id]: {
              ...state.errorDetailsOfItems[action.payload.id],
              isItemQuantityValid: false,
              isItemQuantityNotEmpty: true,
            },
          },
        };
      }
      return {
        ...state,
        itemDetails: state.itemDetails.map((eachItem) => {
          if (eachItem.id === action.payload.id) {
            eachItem.quantity = action.payload.value;
            return eachItem;
          }
          return eachItem;
        }),
        errorDetailsOfItems: {
          ...state.errorDetailsOfItems,
          [action.payload.id]: {
            ...state.errorDetailsOfItems[action.payload.id],
            isItemQuantityValid: "",
            isItemQuantityNotEmpty: false,
          },
        },
      };
    case "itemAdditionalDetails":
      return {
        ...state,
        itemDetails: state.itemDetails.map((eachItem) => {
          if (eachItem.id === action.payload.id) {
            eachItem.additionalDetails = action.payload.value;
            return eachItem;
          }
          return eachItem;
        }),
      };
    case "deleteItem":
      delete state.errorDetailsOfItems[action.payload.id];
      return {
        ...state,
        itemDetails: state.itemDetails.filter(
          (item) => item.id !== action.payload.id
        ),
      };
    case "createItem":
      return {
        ...state,
        itemDetails: [...state.itemDetails, action.payload],
      };
    case "dataCheck":
      let dataCheckStatus = true;
      for (let key in state.errorDetails) {
        if (
          state.errorDetails[key] === false ||
          state.errorDetails[key] === ""
        ) {
          dataCheckStatus = false;
          break;
        }
      }
      for (let key2 in state.errorDetailsOfItems) {
        if (key2) {
          for (let subkey in state.errorDetailsOfItems[key2]) {
            if (state.errorDetailsOfItems[key2][subkey] === false) {
              dataCheckStatus = false;
            }
          }
        }
      }
      if (state.itemDetails.length === 0) {
        dataCheckStatus = false;
      }
      return {
        ...state,
        dataSuccessFull: dataCheckStatus,
      };
    default:
      return state;
  }
};

export default Reducer;
