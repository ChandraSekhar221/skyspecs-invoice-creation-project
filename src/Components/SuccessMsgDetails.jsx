import React from "react";

function SuccessMsgDetails({ state }) {
  return (
    <>
      <div
        className="col-12 d-flex py-2"
        style={{ backgroundColor: "lightGray" }}
      >
        <p className="px-3">Invoice Number : {state.invoiceNumber}</p>
        <p className="px-3">
          Invoice Date :
          {state.invoiceDate.getDate() +
            "-" +
            (parseFloat(state.invoiceDate.getMonth()) + 1 < 10
              ? 0 + (parseFloat(state.invoiceDate.getMonth()) + 1).toString()
              : parseFloat(state.invoiceDate.getMonth()) + 1) +
            "-" +
            state.invoiceDate.getFullYear()}
        </p>
        <p className="px-3">Invoice terms: {state.invoiceTerms}</p>
        {!(
          state.invoiceTerms === "Due On Receipt" ||
          state.invoiceTerms === "None"
        ) && (
          <p className="px-3">
            Invoice Due date:{" "}
            {state.invoiceDueDate.getDate() +
              "-" +
              (parseFloat(state.invoiceDueDate.getMonth()) + 1 < 10
                ? 0 +
                  (parseFloat(state.invoiceDueDate.getMonth()) + 1).toString()
                : parseFloat(state.invoiceDueDate.getMonth()) + 1) +
              "-" +
              state.invoiceDueDate.getFullYear()}
          </p>
        )}
      </div>
      <div className="col-12">
        <hr></hr>
      </div>
      <div className="col-12">
        <div className="row">
          <div className="col-1">
            <p>Sl.No</p>
          </div>
          <div className="col-5">
            <p>Description</p>
          </div>
          <div className="col-2">
            <p>Price</p>
          </div>
          <div className="col-2">
            <p>Quantity</p>
          </div>
          <div className="col-2">
            <p>Amount</p>
          </div>
        </div>
        <div className="col-12">
          <hr className="m-0"></hr>
        </div>
        {state.itemDetails.map((item, index) => {
          return (
            <div className="col-12" key={item.id}>
              <div className="row">
                <div className="col-1">
                  <p>{index + 1}</p>
                </div>
                <div className="col-5">
                  <p>{item.description}</p>
                </div>
                <div className="col-2">
                  <p>{item.price}</p>
                </div>
                <div className="col-2">
                  <p>{item.quantity}</p>
                </div>
                <div className="col-2">
                  <p>
                    ₹{" "}
                    {isNaN((
                      parseFloat(item.price) * parseFloat(item.quantity)
                    ).toFixed(2))
                      ? "0.00"
                      : (
                          parseFloat(item.price) * parseFloat(item.quantity)
                        ).toFixed(2)}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
        <div className="col-12">
          <hr></hr>
        </div>
        <div className="col-12">
          <div className="row">
            <div className="col-5 offset-5 d-flex justify-content-end">
              <p>Total</p>
            </div>
            <div className="col-2">
              <p>
                {state.itemDetails
                  .reduce((prevValue, item) => {
                    let value =
                      parseFloat(item.price) * parseFloat(item.quantity);
                    prevValue += value;
                    return prevValue;
                  }, 0)
                  .toFixed(2)}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div
        className="col-12 d-flex flex-column align-items-start py-2"
        name="successData"
      >
        <p className="m-0 pb-1 text-decoration-underline">From Address :</p>
        <small>
          Name : <span className="h6">{state.from.name}</span>
        </small>
        <small>
          Email : <span className="h6">{state.from.email}</span>
        </small>
        <small>
          Address :{" "}
          <span className="h6">
            {state.from.address +
              ", " +
              state.from.cityState +
              ", " +
              state.from.pincode}
          </span>
        </small>
        <small>
          Phone Number: <span className="h6">{state.from.phone}</span>
        </small>
        <small>
          Business Number:{" "}
          <span className="h6">{state.from.businessNumber}</span>
        </small>
        <p className="m-0 pt-3 pb-1 text-decoration-underline">To Address :</p>
        <small>
          Name : <span className="h6">{state.to.name}</span>
        </small>
        <small>
          Email : <span className="h6">{state.to.email}</span>
        </small>
        <small>
          Address :{" "}
          <span className="h6">
            {state.to.address +
              ", " +
              state.to.cityState +
              ", " +
              state.to.pincode}
          </span>
        </small>
        <small>
          Phone Number: <span className="h6">{state.to.phone}</span>
        </small>
      </div>
    </>
  );
}

export default SuccessMsgDetails;
