export const HrStyles = {
  dotted: "dottedHrLine",
  normal: "normalHrLine",
  solid: "solidHrLine",
};

export const InputFocusStyles = {
  normal: "inputNormal",
  error: "inputError",
};

export const ErrorMsgDetails = {
  invoiceTitleIsEmpty: "* Enter Invoice Title, Should not be Empty",
  fromname: "* Enter Valid Name. Name should only consists of alphabets",
  fromemail: "* Enter Valid Email. examples : abc@gmail.com, hello@co.in",
  fromaddress: "* Address Should be of minimum Five Characters",
  fromcityState: "",
  frompincode: "",
  fromphone: "* Enter Valid Phone Number. Should only consists of Ten digits",
  frombusinessNumber: "* Business Number Should be of minimum Six Alphanumeric Characters",
  toname: "* Enter Valid Name. Name should only consists of alphabets",
  toemail: "* Enter Valid Email. examples : abc@gmail.com, hello@co.in",
  toaddress: "* Address Should be of minimum Five Characters",
  tocityState: "",
  topincode: "",
  tophone: "* Enter Valid Phone Number. Should only consists of Ten digits",
  invoiceNumberinvoiceNumber: "* Invoice Number Should be of minimum Seven Alphanumeric Characters",
};