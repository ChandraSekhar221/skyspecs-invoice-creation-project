import upload from "./upload.png";

function LogoDropDown({ handleEvent, imageDetails }) {
  const handleOnDragOver = (e) => {
    e.preventDefault();
  };

  const handleOnDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const files = e.dataTransfer.files;
    if (files.length) {
      validateFile(files[0]);
    }
  };

  const handleChangeInput = (event) => {
    console.log(event.target.files[0]);
    handleEvent({ type: "dragAndDropImage", payload: event.target.files[0] });
  };

  const validateFile = (file) => {
    const validTypes = [
      "image/jpeg",
      "image/jpg",
      "image/png",
      "image/gif",
      "image/x-icon",
    ];
    if (validTypes.indexOf(file.type) === -1) {
      file["invalid"] = true;
      handleEvent({ type: "dragAndDropImage", payload: file });
    } else {
      handleEvent({ type: "dragAndDropImage", payload: file });
    }
  };

  return (
    <div className="row p-0">
      <div className="col-11 offset-1 px-0">
        <input
          type="file"
          id="input"
          accept="image/*"
          className="d-none"
          onChange={handleChangeInput}
        ></input>
        <div
          className="w-100 border border-secondary"
          onDragOver={handleOnDragOver}
          onDrop={handleOnDrop}
          style={{ cursor: "pointer" }}
        >
          <label htmlFor="input" className="p-5 py-1 cursor">
            <img className="w-25" src={upload} alt="" />
            <p className="h6">Drag & Drop Logos here or click here to select</p>
          </label>
        </div>
      </div>
      {imageDetails && (
        <div className="col-11 offset-1 py-2">
          <div className="row">
            <div className="col-11 d-flex textAlignLeft p-0">
              <img className="w-25" src={imageDetails.url} alt="" />
              <small className="tex-left px-2 align-self-center">
                {imageDetails.name}
              </small>
            </div>
            <div
              className="col-1 file-remove align-self-center"
              onClick={() => handleEvent({ type: "deleteImage" })}
            >
              X
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default LogoDropDown;
