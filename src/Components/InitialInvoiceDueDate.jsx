const InitialInvoiceDueDate = (
  invoiceDate = new Date(),
  invoiceTerms = "2 Days"
) => {
  const invoiceDueDate = new Date(invoiceDate);
  const terms = {
    None: 0,
    Custom: 0,
    "Due On Receipt": 0,
    "Next Day": 1,
    "2 Days": 2,
    "3 Days": 3,
    "4 Days": 4,
    "5 Days": 5,
    "6 Days": 6,
    "7 Days": 7,
  };
  invoiceDueDate.setDate(invoiceDueDate.getDate() + terms[invoiceTerms]);
  //   const upDatedInvoiceDueDate =
  //     invoiceDate.getFullYear() +
  //     "-" +
  //     invoiceDueDate.getMonth() +
  //     "-" +
  //     invoiceDueDate.getDate();
  return invoiceDueDate;
};

export default InitialInvoiceDueDate;
